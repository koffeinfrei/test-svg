## svg relative

![relative](three-dots.svg)

## svg absolute

![absolute](https://gitlab.com/koffeinfrei/test-svg/raw/master/three-dots.svg)


## png relative

![relative](gitlab.png)
